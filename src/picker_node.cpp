//
#include <ros/ros.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <control_msgs/GripperCommandActionGoal.h>
#include <geometric_shapes/solid_primitive_dims.h>


int main(int argc, char **argv){
    ros::init(argc, argv, "pr2_move");
    ros::NodeHandle nodeHandle;
    ros::AsyncSpinner spinner(1);
    spinner.start();
    ros::Publisher gripper_pub = nodeHandle.advertise<control_msgs::GripperCommandActionGoal>
            ("robot/end_effector/right_gripper/gripper_action/goal",1000);
    ros::Publisher pub_co = nodeHandle.advertise<moveit_msgs::CollisionObject>("collision_object", 10);
    ros::Publisher pub_aco = nodeHandle.advertise<moveit_msgs::AttachedCollisionObject>("attached_collision_object", 10);

    moveit::planning_interface::MoveGroup group("right_arm");
    group.setPlanningTime(45.0);
    group.setPlannerId("RRTConnectkConfigDefault");

    ROS_INFO("Reference frame: %s", group.getEndEffectorLink().c_str());
    control_msgs::GripperCommandActionGoal start_grip;

    start_grip.goal.command.position = 100;
    start_grip.goal.command.max_effort=5;
    gripper_pub.publish(start_grip);

    moveit::planning_interface::MoveGroup::Plan start_plan;

    geometry_msgs::Pose start_pose;
    start_pose.orientation.x = 0.707;
    start_pose.orientation.y = 0;
    start_pose.orientation.z = 0.707;
    start_pose.orientation.w = 0;
    start_pose.position.x = 0.9;
    start_pose.position.y = -0.1;
    start_pose.position.z = 0.25;
    group.setPoseTarget(start_pose);

    group.plan(start_plan);
    sleep(5.0);

    group.move();

    // add object
    moveit_msgs::CollisionObject co;
    co.header.stamp = ros::Time::now();
    co.header.frame_id = "base";
    co.id = "part";
    co.operation = moveit_msgs::CollisionObject::REMOVE;
    pub_co.publish(co);


    moveit_msgs::AttachedCollisionObject aco;
    aco.object = co;


    co.operation = moveit_msgs::CollisionObject::ADD;
    co.primitives.resize(1);
    co.primitives[0].type = shape_msgs::SolidPrimitive::BOX;
    co.primitives[0].dimensions.resize(geometric_shapes::SolidPrimitiveDimCount<shape_msgs::SolidPrimitive::BOX>::value);
    co.primitives[0].dimensions[shape_msgs::SolidPrimitive::BOX_X] = 0.15;
    co.primitives[0].dimensions[shape_msgs::SolidPrimitive::BOX_Y] = 0.02;
    co.primitives[0].dimensions[shape_msgs::SolidPrimitive::BOX_Z] = 0.3;
    co.primitive_poses.resize(1);
    co.primitive_poses[0].position.x = 0.93;
    co.primitive_poses[0].position.y = -0.1;
    co.primitive_poses[0].position.z = 0.2;
    co.primitive_poses[0].orientation.w = 1.0;

    pub_co.publish(co);
    aco.touch_links.push_back("right_gripper");
    aco.touch_links.push_back("r_gripper_r_finger_tip");
    aco.touch_links.push_back("r_gripper_l_finger_tip");
    aco.touch_links.push_back("r_gripper_l_finger");
    aco.touch_links.push_back("r_gripper_r_finger");
    ros::WallDuration(1).sleep();
    pub_aco.publish(aco);
    group.attachObject("part");

    start_grip.goal.command.position = 0;
    start_grip.goal.command.max_effort=5;
    gripper_pub.publish(start_grip);



    start_pose.position.z += 0.06;
    start_pose.position.x += -0.06;
    group.setPoseTarget(start_pose);

    group.plan(start_plan);
    sleep(5.0);

    group.move();
    ros::waitForShutdown();
    return 0;
}
